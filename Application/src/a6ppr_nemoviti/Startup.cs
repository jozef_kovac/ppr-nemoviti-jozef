using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using a6ppr_nemoviti.Domain.Constants;
using a6ppr_nemoviti.Infrastructure.Identity.Roles;
using a6ppr_nemoviti.Infrastructure.Identity.Users;
using a6ppr_nemoviti.Application.Configuration;
using a6ppr_nemoviti.Domain.Entities.Settings;

namespace a6ppr_nemoviti
{
    public class Startup
    {

        public IHostingEnvironment Environment { get; set; }
        public IContainer Container { get; private set; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            ServiceConfiguration.Load(services, Environment);
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
            return ConfigureAutofacContainer(services);
        }

        private IServiceProvider ConfigureAutofacContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ApplicationDependencyModule>();
            builder.Populate(services);

            Container = builder.Build();
            return new AutofacServiceProvider(Container);
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller}/{action}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{area=Client}/{controller}/{action=Index}/{id?}");
            });
            EnsureRolesCreated(app).ConfigureAwait(false);

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }

        private async Task EnsureRolesCreated(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();
                string[] roles = { Roles.User, Roles.Manager, Roles.Admin };

                foreach (var role in roles)
                {
                    var roleExists = await roleManager.RoleExistsAsync(role);
                    if (!roleExists)
                        await roleManager.CreateAsync(new Role(role));
                }

                var admin = new User
                {
                    UserName = "admin@utb.cz",
                    Email = "admin@utb.cz",
                    EmailConfirmed = true
                };
                var password = "Heslo_123";

                var user = await userManager.FindByEmailAsync(admin.Email);
                if (user == null)
                {
                    var createdUser = await userManager.CreateAsync(admin, password);
                    if (createdUser.Succeeded)
                    {
                        foreach (var role in roles)
                        {
                            await userManager.AddToRoleAsync(admin, role);
                        }
                    }
                }
            }
        }
    }
}
