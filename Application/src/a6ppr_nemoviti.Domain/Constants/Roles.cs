﻿namespace a6ppr_nemoviti.Domain.Constants
{
    public class Roles
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string Manager = "Manager";
    }
}
