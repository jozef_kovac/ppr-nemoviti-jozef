﻿using System;
using System.Collections.Generic;

namespace a6ppr_nemoviti.Domain.Repositories
{
    public interface IRepository<TEntity>
    {
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Remove(TEntity entity);
        TEntity Get(Func<TEntity, bool> predicate);
        IList<TEntity> GetAll();
        IList<TEntity> GetAll(Func<TEntity, bool> predicate);
    }
}
