﻿namespace a6ppr_nemoviti.Domain.Utils
{
    public static class Utils
    {
        public static string ShortenString(string input, int max)
        {
            var shortened = input;
            if (!string.IsNullOrEmpty(input) && input.Length > max)
            {
                shortened = input.Substring(0, max);
            }
            return shortened.Length < input.Length ? $"{shortened}..." : input;
        }
    }
}
