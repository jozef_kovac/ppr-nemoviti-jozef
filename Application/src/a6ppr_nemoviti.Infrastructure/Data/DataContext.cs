﻿using a6ppr_nemoviti.Domain.Entities;
using a6ppr_nemoviti.Infrastructure.Extensions;
using a6ppr_nemoviti.Infrastructure.Identity.Roles;
using a6ppr_nemoviti.Infrastructure.Identity.Users;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace a6ppr_nemoviti.Infrastructure.Data
{
    public class DataContext : IdentityDbContext<User, Role, int>
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.UseEntityTypeConfiguration();

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName =
                    entity.Relational().TableName.Replace("AspNet", string.Empty);
            }
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            var changedEntities = ChangeTracker.Entries().Where(e => e.Entity is Entity
                                                && (e.State == EntityState.Modified ||
                                                    e.State == EntityState.Added ||
                                                    e.State == EntityState.Deleted));

            foreach (var item in changedEntities)
            {
                var entity = item.Entity as Entity;

                if (item.State == EntityState.Added)
                {
                    entity.DateCreated = DateTime.Now;
                }
                entity.DateUpdated = DateTime.Now;
            }

            ChangeTracker.AutoDetectChangesEnabled = false;
            var result = base.SaveChanges();
            ChangeTracker.AutoDetectChangesEnabled = true;

            return result;
        }
    }
}
