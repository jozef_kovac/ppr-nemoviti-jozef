﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace a6ppr_nemoviti.Infrastructure.Data
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseSqlServer("Server=databaze.fai.utb.cz;"
                + "Database=A17561_A5PWT_Projekt;"
                + "User ID=A17561;"
                + "Password=A17561;"
                + "persist security info=True;"
                + "multipleactiveResultsets=True;");
            return new DataContext(builder.Options);
        }
    }
}
