﻿using a6ppr_nemoviti.Infrastructure.Data;
using a6ppr_nemoviti.Infrastructure.Identity.Roles;
using a6ppr_nemoviti.Infrastructure.Identity.Users;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace a6ppr_nemoviti.Infrastructure.Configuration
{
    public class InfrastructureServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment environment)
        {
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlServer("Server=databaze.fai.utb.cz;"
                                    + "Database=A17561_A5PWT_Projekt;"
                                    + "User ID=A17561;"
                                    + "Password=A17561;"
                                    + "persist security info=True;"
                                    + "multipleactiveResultsets=True;");
            });

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;

                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.LoginPath = "/Admin/Security/Login";
                options.LogoutPath = "/Admin/Security/Logout";
                options.SlidingExpiration = true;
            });
        }
    }
}
