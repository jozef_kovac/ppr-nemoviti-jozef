﻿using Microsoft.AspNetCore.Identity;

namespace a6ppr_nemoviti.Infrastructure.Identity.Roles
{
    public class Role : IdentityRole<int>
    {
        public Role(string name) : base(name)
        {
        }
    }
}
