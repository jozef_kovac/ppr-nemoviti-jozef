﻿namespace a6ppr_nemoviti.Application.Client.ViewModels
{
    public class BaseViewModel
    {
        public int? UserID { get; set; }
        public string Login { get; set; }
    }
}
