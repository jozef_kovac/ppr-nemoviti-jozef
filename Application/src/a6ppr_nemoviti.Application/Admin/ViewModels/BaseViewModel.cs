﻿namespace a6ppr_nemoviti.Application.Admin.ViewModels
{
    public class BaseViewModel
    {
        public int? UserID { get; set; }
        public string Login { get; set; }
    }
}
