﻿using a6ppr_nemoviti.Infrastructure.Configuration;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace a6ppr_nemoviti.Application.Configuration
{
    public class ServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment environment)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(ServiceConfiguration)));
            services.AddSingleton(environment);
            InfrastructureServiceConfiguration.Load(services, environment);
        }
    }
}
