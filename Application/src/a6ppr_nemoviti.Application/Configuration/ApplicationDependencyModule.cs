﻿using a6ppr_nemoviti.Domain.Configuration;
using a6ppr_nemoviti.Infrastructure.Configuration;
using Autofac;

namespace a6ppr_nemoviti.Application.Configuration
{
    public class ApplicationDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterModule<DomainDependencyModule>();
            builder.RegisterModule<InfrastructureDependencyModule>();
        }
    }
}
